<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Locations extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'street',
        'zip_code',
        'city',
        'country',
        'url',
        'latitude',
        'longitude'
    ];

    protected $guarded = []; 
}
