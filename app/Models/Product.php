<?php

namespace App\Models;

use Cknow\Money\Casts\MoneyIntegerCast;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $casts = [

        'price' => MoneyIntegerCast::class . ':EUR',
    ];

    protected $fillable = [
        'name',
        'description',
        'img_path',
        'price',
        'qty'
    ];

    protected $guarded = []; 
}
