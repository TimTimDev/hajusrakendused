<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Cknow\Money\Money;
use Illuminate\Http\Request;
use Inertia\Inertia;


class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return Inertia::render('Products', [
            "products" => $products
        ]);
    }

    public function renderDetails($id)
    {
        $product = Product::findOrFail($id);
        return Inertia::render('ProductDetails', [
            "product" => $product
        ]);
    }
    
    public function renderHome()
    {
        $products = Product::all();
        return Inertia::render('Admin/Dashboard', [
            "products" => $products
        ]);
    }

    public function renderProducts()
    {
        $products = Product::all();
        return Inertia::render('Admin/Products', [
            "products" => $products
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'          => "required",
            'description'   => "required",
            'img_path'      => "required",
            'price'         => "required|numeric",
            'qty'           => "required|numeric|gt:0",
        ]);

        $price = Money::parseByDecimal($request->price, 'EUR');


        Product::create([
            'name'          => $request->name,
            'description'   => $request->description,
            'img_path'      => $request->img_path,
            'price'         => $price,
            'qty'           => $request->qty
        ]);

        return redirect()->back();
    }

    public function edit($id)
    {
        return Inertia::render('Admin/EditProduct', [
            "product" => Product::find($id)
        ]);
    }

    public function update(Request $request, $id)
    {
        $editProduct = Product::findOrFail($id);
        
        $editProduct->update($request->validate([
            'name'          => "required",
            'description'   => "required",
            'img_path'      => "required",
            'price'         => "required|numeric",
            'qty'           => "required|numeric|gt:0",
        ]));

        return redirect()->route("admin.product.index");

    }

    public function destroy($id)
    {
        $delProduct = Product::findOrFail($id);

        $delProduct->delete();

        return redirect()->back();

        // dd($id);
    }
}
