<?php

namespace App\Http\Controllers;

use App\Models\Locations;
use Illuminate\Http\Request;
use Inertia\Inertia;

class LocationsController extends Controller
{
    
    public function index()
    {
        $locations = Locations::all();
        return Inertia::render('Admin/Locations', [
            "locations" => $locations
        ]);
    }

    public function renderLocations()
    {
        $locations = Locations::all();
        return Inertia::render('Locations', [
            "locations" => $locations
        ]);
    }

    public function create()
    {
        return Inertia::render('Admin/AddLocations');
    }
    
    public function store(Request $request)
    {
        Locations::create($request->validate([
            'name'          => "required",
            'street'        => "required",
            'zip_code'      => "required|numeric",
            'city'          => "required",
            'country'       => "required",
            'url'           => "required",
            'latitude'      => "required|numeric",
            'longitude'     => "required|numeric",
        ]));

        return redirect()->back();
    }

    
    public function edit($id)
    {
        return Inertia::render('Admin/EditLocations', [
            "location" => Locations::find($id)
        ]);
    }

    
    public function update(Request $request, $id)
    {
        $editLocation = Locations::findOrFail($id);
        
        $editLocation->update($request->validate([
            'name'          => "required",
            'street'        => "required",
            'zip_code'      => "required",
            'city'          => "required",
            'country'       => "required",
            'url'           => "required",
            'latitude'      => "required|numeric",
            'longitude'     => "required|numeric",
        ]));

        return redirect()->route("admin.locations.index");

    }

    
    public function destroy($id)
    {
        $delLocation = Locations::findOrFail($id);

        $delLocation->delete();

        return redirect()->back();
    }
}
