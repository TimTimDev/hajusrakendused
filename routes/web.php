<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\LocationsController;
use App\Http\Controllers\WeatherApiController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\ShopController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

require __DIR__.'/auth.php';

Route::get('/', function () {
    return Inertia::render('Home');
})->name('home');

Route::get('/contact', function () {
    return Inertia::render('Contact');
})->name('contact');

Route::get('/cart', function () {
    return Inertia::render('Cart');
})->name('cart');


Route::get('/products', [ProductController::class, 'index'])->name('product.index');
Route::get('/products/{id}', [ProductController::class, 'renderDetails'])->name('product.details');

// Route::get('/products/{id}', [ShopController::class, 'cartAdd'])->name('cart.product.add');

Route::get('/dealers', [LocationsController::class, 'renderLocations'])->name('dealers');

Route::get('/weather', [WeatherApiController::class, 'index'])->name('weather');

Route::get('/blog', [BlogController::class, 'index'])->name('blog');