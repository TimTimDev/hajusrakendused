<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\EmailVerificationNotificationController;
use App\Http\Controllers\Auth\EmailVerificationPromptController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\Auth\VerifyEmailController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\LocationsController;
use App\Http\Controllers\BlogController;
use App\Models\Product;
use Illuminate\Support\Facades\Route;

Route::middleware('guest')->group(function () {
    Route::get('register', [RegisteredUserController::class, 'create'])
                ->name('register');

    Route::post('register', [RegisteredUserController::class, 'store']);

    Route::get('login', [AuthenticatedSessionController::class, 'create'])
                ->name('login');

    Route::post('login', [AuthenticatedSessionController::class, 'store']);

    Route::get('forgot-password', [PasswordResetLinkController::class, 'create'])
                ->name('password.request');

    Route::post('forgot-password', [PasswordResetLinkController::class, 'store'])
                ->name('password.email');

    Route::get('reset-password/{token}', [NewPasswordController::class, 'create'])
                ->name('password.reset');

    Route::post('reset-password', [NewPasswordController::class, 'store'])
                ->name('password.update');
});

Route::middleware('auth')->group(function () {
    Route::get('verify-email', [EmailVerificationPromptController::class, '__invoke'])
                ->name('verification.notice');

    Route::get('verify-email/{id}/{hash}', [VerifyEmailController::class, '__invoke'])
                ->middleware(['signed', 'throttle:6,1'])
                ->name('verification.verify');

    Route::post('email/verification-notification', [EmailVerificationNotificationController::class, 'store'])
                ->middleware('throttle:6,1')
                ->name('verification.send');

    Route::get('confirm-password', [ConfirmablePasswordController::class, 'show'])
                ->name('password.confirm');

    Route::post('confirm-password', [ConfirmablePasswordController::class, 'store']);

    Route::post('logout', [AuthenticatedSessionController::class, 'destroy'])
                ->name('logout');

            });
            
Route::middleware('auth')->prefix('admin/')->name('admin.')->group(function () {
    
    Route::get('/dashboard', [ProductController::class, "renderHome"])->name('dashboard.index');
    Route::get('/dashboard/products', [ProductController::class, 'renderProducts'])->name('product.index');
    Route::get('/dashboard/products/edit/{id}', [ProductController::class, 'edit'])->name('product.edit');
    Route::post('/dashboard/products/update/{id}', [ProductController::class, 'update'])->name('product.update');
    Route::post('/dashboard/product-store', [ProductController::class, 'store'])->name('product.store');
    Route::post('/dashboard/products/delete/{id}', [ProductController::class, 'destroy'])->name('product.destroy');

    Route::get('/dashboard/locations', [LocationsController::class, 'index'])->name('locations.index');
    Route::get('/dashboard/location/edit/{id}', [LocationsController::class, 'edit'])->name('locations.edit');
    Route::post('/dashboard/location/update/{id}', [LocationsController::class, 'update'])->name('locations.update');
    Route::get('/dashboard/location/create', [LocationsController::class, 'create'])->name('locations.create');
    Route::post('/dashboard/location/store', [LocationsController::class, 'store'])->name('locations.store');
    Route::post('/dashboard/locations/delete/{id}', [LocationsController::class, 'destroy'])->name('locations.destroy');

    //show posts
    Route::get('/blog-list', [BlogController::class, 'show'])->name('blog.index');
    //add post
    Route::get('/blog-add', [BlogController::class, 'create'])->name('blog.add');
    Route::post('/blog-add', [BlogController::class, 'store']);
    //edit post
    Route::get('/blog-edit/{blog}', [BlogController::class, 'edit'])->name('blog.edit');
    Route::post('/blog-edit/{blog}', [BlogController::class, 'update'])->name('blog.update');
    //delete post
    Route::post('/blog-delete/{blog}', [BlogController::class, 'destroy'])->name('blog.delete');

});
