<?php

namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'Paiste shop');
set('remote_user', 'virt98396'); //virt...
set('http_user', 'virt98396');
set('keep_releases', 2);

// Hosts
host('ta20pirn.itmajakas.ee')
    ->setHostname('ta20pirn.itmajakas.ee')
    ->set('http_user', '')
    ->set('deploy_path', '~/domeenid/www.ta20pirn.itmajakas.ee/paiste-pood')
    ->set('branch', 'master');

// Tasks
set('repository', 'git@gitlab.com:TimTimDev/hajusrakendused.git');
//Restart opcache
task('opcache:clear', function () {
    run('killall php80-cgi || true');
})->desc('Clear opcache');

task('build:node', function () {
    cd('{{release_path}}');
    run('npm i');
    run('npx vite build');
    run('rm -rf node_modules');
});
task('deploy', [
    'deploy:prepare',
    'deploy:vendors',
    'artisan:storage:link',
    'artisan:view:cache',
    'artisan:config:cache',
    'build:node',
    'deploy:publish',
    'opcache:clear',
    'artisan:cache:clear'
]);
after('deploy:failed', 'deploy:unlock');
