<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            "name" => $this->faker->words(4, true),
            "description" => $this->faker->paragraph(4),
            "img_path" => $this->faker->imageUrl(640, 480, null, true, null, true),
            "price" => $this->faker->randomNumber(5, true),
            "qty" => $this->faker->numberBetween(0, 50)
        ];
    }
}
